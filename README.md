# React calculator

![](calculator.png)

Realizzare una calcolatrice utilizzando React.

Basarsi sull'immagine qui sopra per avere uno spunto.

Testare l'app con Jest e Enzyme

## Hint

Se volete potete usare la calcolatrice dell'esercizio [sulla calcolatrice in TDD](../tdd-calculator) per fare i conti
