import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";

import Display from "./Display";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Display />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Shows values", () => {
  const wrapper = shallow(<Display value="5.12" />);
  expect(wrapper.find("div.display").text()).toBe("5.12");
});
