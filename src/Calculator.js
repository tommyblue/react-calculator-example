import React from "react";
import Display from "./Display";
import Button from "./Button";
import { calculate } from "./calculations";
import { isOperator, isValue, isEqual, isClear, isDecimal } from "./utils";

import "./Calculator.scss";

const BUTTONS = [
  ["+", "operator"],
  ["-", "operator"],
  ["*", "operator"],
  ["/", "operator"],
  ["7", "number"],
  ["8", "number"],
  ["9", "number"],
  ["4", "number"],
  ["5", "number"],
  ["6", "number"],
  ["1", "number"],
  ["2", "number"],
  ["3", "number"],
  ["0", "number"],
  [".", "decimal"],
  ["AC", "all-clear"],
  ["=", "equal-sign"]
];

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      result: null,
      operator: null,
      v1: null,
      v2: null
    };
    this.clickButton = this.clickButton.bind(this);
    this.updateResult = this.updateResult.bind(this);
    this.getOperation = this.getOperation.bind(this);
    this.getDisplayValue = this.getDisplayValue.bind(this);
  }

  render() {
    return (
      <div className="Calculator">
        <Display value={this.getDisplayValue()} />
        <div className="calculator-keys">
          {BUTTONS.map((b, i) => (
            <Button
              key={i}
              value={b[0]}
              class={b[1]}
              onClick={this.clickButton}
            />
          ))}
        </div>
      </div>
    );
  }

  getOperation() {
    return `${this.state.v1 || "0"}${this.state.operator || ""}${this.state
      .v2 || ""}`;
  }

  getDisplayValue() {
    return this.state.result !== null ? this.state.result : this.getOperation();
  }

  clickButton(value) {
    let { operator, v1, v2 } = this.state;

    if (isOperator(value)) {
      operator = value;
      if (v1 === null) {
        v1 = "0";
      }
      this.setState({ result: null, operator, v1, v2 });
      return;
    }

    if (isValue(value)) {
      if (operator === null) {
        v1 = `${v1 || ""}${value}`;
      } else {
        v2 = `${v2 || ""}${value}`;
      }
      this.setState({
        result: null,
        operator,
        v1,
        v2
      });
      return;
    }

    if (isClear(value)) {
      this.setState({
        result: null,
        operator: null,
        v1: null,
        v2: null
      });
      return;
    }

    if (isEqual(value)) {
      this.updateResult();
      return;
    }

    if (isDecimal(value)) {
      if (v1 === null || (v2 === null && operator !== null)) {
        return;
      }
      if (v2 === null) {
        v1 = `${v1}.`;
      } else {
        v2 = `${v2}.`;
      }
      this.setState({
        operator,
        v1,
        v2
      });
      return;
    }

    throw new Error(`Unexpected button ${value}`);
  }

  updateResult() {
    let result = calculate(this.getOperation());
    // Fix leading .00
    if (result.substring(result.length - 3, result.length) === ".00") {
      result = parseInt(parseFloat(result)).toString();
    }
    this.setState({
      result,
      operator: null,
      v1: result,
      v2: null
    });
  }
}
