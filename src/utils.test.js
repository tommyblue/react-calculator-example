import { isOperator, isValue, isEqual, isClear, isDecimal } from "./utils";

test("identify operators", () => {
  expect(isOperator("+")).toBeTruthy();
  expect(isOperator("-")).toBeTruthy();
  expect(isOperator("*")).toBeTruthy();
  expect(isOperator("/")).toBeTruthy();
  expect(isOperator("5")).toBeFalsy();
  expect(isOperator("=")).toBeFalsy();
});

test("identify values", () => {
  expect(isValue("+")).toBeFalsy();
  expect(isValue("0")).toBeTruthy();
  expect(isValue("5")).toBeTruthy();
  expect(isValue("=")).toBeFalsy();
});

test("identify equal", () => {
  expect(isEqual("-")).toBeFalsy();
  expect(isEqual("5")).toBeFalsy();
  expect(isEqual("=")).toBeTruthy();
});

test("identify clear button", () => {
  expect(isClear("AC")).toBeTruthy();
  expect(isClear("-")).toBeFalsy();
  expect(isClear("*")).toBeFalsy();
  expect(isClear("/")).toBeFalsy();
  expect(isClear("5")).toBeFalsy();
  expect(isClear("=")).toBeFalsy();
});

test("identify decimal button", () => {
  expect(isDecimal(".")).toBeTruthy();
  expect(isDecimal("AC")).toBeFalsy();
  expect(isDecimal("5")).toBeFalsy();
  expect(isDecimal("=")).toBeFalsy();
});
