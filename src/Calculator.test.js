import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";

import Calculator from "./Calculator";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Calculator />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Returns a formatted operation", () => {
  const wrapper = shallow(<Calculator />);
  wrapper.setState({
    v1: "4.12",
    operator: "*",
    v2: "2"
  });
  expect(wrapper.instance().getOperation()).toBe("4.12*2");
});

it("Can display a value", () => {
  const wrapper = shallow(<Calculator />);
  wrapper.setState({
    result: null,
    v1: "4.12",
    operator: "*",
    v2: "2"
  });
  expect(wrapper.instance().getDisplayValue()).toBe("4.12*2");

  wrapper.setState({
    result: "6/2",
    v1: "4.12",
    operator: "*",
    v2: "2"
  });
  expect(wrapper.instance().getDisplayValue()).toBe("6/2");
});

it("Can click buttons", () => {
  const wrapper = shallow(<Calculator />);
  const state = {
    result: null,
    operator: null,
    v1: null,
    v2: null
  };
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("4");
  state.v1 = "4";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton(".");
  state.v1 = "4.";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("5");
  state.v1 = "4.5";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("+");
  state.operator = "+";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("6");
  state.v2 = "6";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton(".");
  state.v2 = "6.";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("2");
  state.v2 = "6.2";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("=");
  state.result = "10.70";
  state.operator = null;
  state.v1 = "10.70";
  state.v2 = null;
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("-");
  state.result = null;
  state.operator = "-";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("0");
  state.v2 = "0";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton(".");
  state.v2 = "0.";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("7");
  state.v2 = "0.7";
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("=");
  state.result = "10";
  state.operator = null;
  state.v1 = "10";
  state.v2 = null;
  expect(wrapper.state()).toMatchObject(state);

  wrapper.instance().clickButton("AC");
  state.result = null;
  state.operator = null;
  state.v1 = null;
  state.v2 = null;
  expect(wrapper.state()).toMatchObject(state);
});
