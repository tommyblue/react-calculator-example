export const isOperator = v => {
  return ["+", "-", "*", "/"].indexOf(v) !== -1;
};

export const isValue = v => {
  return ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].indexOf(v) !== -1;
};

export const isEqual = v => {
  return v === "=";
};

export const isClear = v => {
  return v === "AC";
};

export const isDecimal = v => {
  return v === ".";
};
