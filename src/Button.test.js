import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";

import Button from "./Button";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Button />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Renders buttons", () => {
  const wrapper = shallow(<Button value="5" />);
  expect(wrapper.find("button").text()).toBe("5");
});

it("Can click buttons", () => {
  const fn = jest.fn();
  const wrapper = shallow(<Button value="5" onClick={fn} />);
  wrapper.simulate("click");
  wrapper.simulate("click");
  expect(fn).toHaveBeenCalledTimes(2);
  expect(fn).toHaveBeenLastCalledWith("5");
});
