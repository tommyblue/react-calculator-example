export const calculate = input => {
  const [e1, op, e2] = parseString(input);

  const operations = {
    "+": sum,
    "-": diff,
    "*": mult,
    "/": div
  };
  return operations[op](parseFloat(e1), parseFloat(e2)).toFixed(2);
};

export const parseString = input => {
  const regex = /(\d+[.\d]*)(\+|-|\*|\/)(\d+[.\d]*)/gm;
  return input.split(regex).filter(el => el);
};

const sum = (a, b) => a + b;
const diff = (a, b) => a - b;
const mult = (a, b) => a * b;
const div = (a, b) => a / b;
