import { calculate, parseString } from "./calculations";

test("Calculate simple sum", () => {
  expect(calculate("1+1")).toBe("2.00");
  expect(calculate("9.21-4.11")).toBe("5.10");
  expect(calculate("3.98*4")).toBe("15.92");
  expect(calculate("10/3.0")).toBe("3.33");
});

test("Parse string", () => {
  const tt = [
    ["1+1", ["1", "+", "1"]],
    ["1-2.3", ["1", "-", "2.3"]],
    ["1.31-2.3", ["1.31", "-", "2.3"]]
  ];

  tt.forEach(t => {
    expect(parseString(t[0])).toEqual(t[1]);
  });
});
